DIR=./src/
CXX=g++
CXXFLAGS= -O3 -Wall -std=c++11 -lrt
OBJS= ${DIR}graph.o ${DIR}bk_state.o ${DIR}util.o
HEADS= ${DIR}bk_state.h ${DIR}graph.h


all: search convert

search: ${DIR}main.cpp ${OBJS} ${HEADS}
	${CXX} -o $@ $< ${OBJS} ${CXXFLAGS}

convert: ${DIR}convert.cpp ${DIR}graph.o ${DIR}graph.h
	${CXX} -o $@ $< ${DIR}graph.o ${CXXFLAGS}

%.o: %.cpp %.h
	$(CXX) -o $@ -c $< $(CXXFLAGS)

clean:
	rm -f ${DIR}*.o search convert
