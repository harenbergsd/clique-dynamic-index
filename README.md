Query-driven Clique Enumeration with Dynamic Indexing
=====================================================

Building the program
--------------------
You will need g++ with the c++11 standard, then simply run `make`.


Running the program
-------------------
First, you have to convert the graph to binary format using the `convert` executable. Note: your edgelist does NOT need to have consecutive IDs. Some example data can be found in `data/`.

To convert a graph, use e.g.:
```
$ ./convert data/small.graph -o small.bin
```

When performing the algorithm, you can optionally specify a query file or files to perform. The query file should be a text file with the first line specifying the minimum number of query nodes that a clique must contain and the remaining lines specifying the query nodes. As an example, see `data/small.q`.

To run the code, use e.g.:
```
./search data/small.bin -q small.q 
```

In this case, a single `.cliques` text file will be created containing the output. For a full list of arguments that can be passed to the search code, run `./search` (no arguments).
