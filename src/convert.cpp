#include <iostream>
#include "graph.h"


std::string graph_path = "";
std::string output_path = "graph.bin";


void usage(char *prog_name, const char *more)
{
    std::cerr << more;
    std::cerr << "usage: " << prog_name << " <graph_path> [-o output_path]\n" << std::endl;
    std::cerr << "-o path to output results" << std::endl;
    exit(0);
}


void parse_args(int argc, char **argv)
{
    if (argc<1)
        usage(argv[0], "Incorrect number of arguments\n");
    
    for (int i=1; i<argc; i++) 
    {
        if(argv[i][0] == '-') 
        {
            switch(argv[i][1])
            {
                case 'o':
                    output_path = argv[++i];
                    break;
            }
        } 
        else 
            if (graph_path == "")
                graph_path = argv[i];
            else
                usage(argv[0], "More than one filename\n");
    }
    if (graph_path == "")
        usage(argv[0], "No input file has been provided\n");
}


int main(int argc, char* argv[])
{
    parse_args(argc, argv);

    Graph graph(graph_path, false);
    graph.write_binary(output_path);
    
    return 0;
}
