#include "bk_state.h"

extern Graph graph;
extern bool no_index;


/* A non-root states of the BK algorithm */
BK_State::BK_State(std::unordered_set<vertex> &R, 
                   std::unordered_set<vertex> &P, 
                   std::unordered_set<vertex> &X)
{
    this->R = R;
    this->P = P;
    this->X = X;
}

BK_State::BK_State(const std::vector<vertex> &R, 
                   const std::vector<vertex> &P, 
                   const std::vector<vertex> &X)
{
    this->R = std::unordered_set<vertex>(R.begin(), R.end());
    this->P = std::unordered_set<vertex>(P.begin(), P.end());
    this->X = std::unordered_set<vertex>(X.begin(), X.end());
}


BK_State::~BK_State(){}   // destructor doesn't need anything special


vertex BK_State::select_pivot(std::unordered_set<vertex>& P, std::unordered_set<vertex>& X)
{
    // Combine P and X sets
    std::unordered_set<vertex> PX(P);
    PX.insert(X.begin(), X.end());
    
    vertex curr_pivot = 0;
    vertex curr_degree = 0;
    for (vertex v : PX) 
    {
        vertex degree = util::intersection(graph.neighbors(v),P).size();
        if (degree >= curr_degree) 
        {
            curr_pivot = v;
            curr_degree = degree;
        }
    }
    return curr_pivot;
}
        

std::list<BK_State*> BK_State::successors(Query &Q)
{
    std::list<BK_State*> children;   // holds the child states
    
    if (P.size() == 0)              // no more candidate vertices to add
        return children;

    // Split P into query and non-query nodes
    std::unordered_set<vertex> PQ = util::intersection(P, Q.verts);
    std::unordered_set<vertex> PnoQ = util::difference(P, PQ);

    // Perform P \ N(u), where u is the pivot vertex
    vertex u = select_pivot(PnoQ, X);
    std::unordered_set<vertex>& nbs = graph.neighbors(u);
    std::unordered_set<vertex> diff = util::difference(PnoQ, nbs);
    
    // Generate all the child states, iterating over query nodes first
    std::vector<vertex> verts(PQ.size()+diff.size());
    std::copy(PQ.begin(), PQ.end(), verts.begin());
    std::copy(diff.begin(), diff.end(), verts.begin()+PQ.size());
    for (vertex v : verts)
    {
        std::unordered_set<vertex>& nbs = graph.neighbors(v);
        
        // Build the new R, P, and X sets
        R.insert(v);
        std::unordered_set<vertex> Pnew = util::intersection(nbs, P);
        std::unordered_set<vertex> Xnew = util::intersection(nbs, X);
        children.push_back(new BK_State(R, Pnew, Xnew));

        R.erase(v);   // we will reuse this set, so remove recently pushed v
        P.erase(v);
        X.insert(v);
    }
    
    return children;
}


void BK_State::output(std::ofstream &file)
{
    vertex count = 0;
    for (vertex v : R)
    {
        count ++;
        file << v;
        if (count != R.size())
            file << " ";
    }
    file << std::endl;
}


std::list<BK_State*> BK_State::roots_for_seeds(std::unordered_set<vertex>& verts, std::unordered_set<vertex> &X)
{
    std::list<BK_State*> states;
    for (vertex v : verts)
    {
        std::unordered_set<vertex> R = {v};
        std::unordered_set<vertex>& nbs = graph.neighbors(v);
        
        std::unordered_set<vertex> P = util::difference(nbs, X);
        std::unordered_set<vertex> Xnew = util::intersection(nbs, X);
        states.push_back(new BK_State(R,P,Xnew));
        X.insert(v);
    }
    return states;
}


void BK_State::remove_from(std::unordered_map<vertex,std::vector<BK_State*> >& inv_index)
{
    //for (vertex v : R)
    //    inv_index[v].erase(this);
    //for (vertex v : P)
    //    inv_index[v].erase(this);
    for (vertex v : R)
    {
        auto position = std::find(inv_index[v].begin(), inv_index[v].end(), this);
        if (position != inv_index[v].end())
            inv_index[v].erase(position);
    }
    for (vertex v : P)
    {
        auto position = std::find(inv_index[v].begin(), inv_index[v].end(), this);
        if (position != inv_index[v].end())
            inv_index[v].erase(position);
    }
}


std::list<BK_State*> BK_State::initialize(Query &Q, 
                                       std::unordered_map<vertex,std::vector<BK_State*> >& inv_index, 
                                       std::unordered_set<vertex>& prev_seeds)
{
    std::list<BK_State*> frontier;
    
    // If no queries have been processed and no query given (i.e., full enumeration),
    // return the single highest root node.
    if (Q.verts.size()==0 && inv_index.size()==0)
    {
        std::unordered_set<vertex> R, X;
        std::unordered_set<vertex> P = graph.get_verts();
        frontier.push_back(new BK_State(R,P,X));
        
        prev_seeds.insert(P.begin(), P.end());
        return frontier;
    }

    // Assume that no query vertices means full enumeration
    if (Q.verts.size() == 0)
        Q.verts = graph.get_verts();
    
    // Determine states that haven't been expanded from topmost root
    std::unordered_set<vertex> new_seeds = util::difference(Q.verts, prev_seeds);
    frontier = roots_for_seeds(new_seeds, prev_seeds);
    if (no_index)
    {
        prev_seeds.clear();
        return frontier;
    }
    
    // Get the indexed states that are relevant to the query
    std::unordered_map<BK_State*, vertex> counts;
    for (vertex v : Q.verts)
    {
        for (BK_State* state : inv_index[v])
        {
            if (counts.find(state) == counts.end())
                counts.insert({state, 0});
            counts[state]++;
        }
    }
    for (auto state_count : counts)
        if (state_count.second >= Q.eta)
            frontier.push_back(state_count.first);
    return frontier;
}



void BK_State::display()
{
    std::cout << "R:";
    for (vertex v : R)
        std::cout << " " << v;
    std::cout << "\nP:";
    for (vertex v : P)
        std::cout << " " << v;
    std::cout << "\nX:";
    for (vertex v : X)
        std::cout << " " << v;
    std::cout << std::endl;
}
