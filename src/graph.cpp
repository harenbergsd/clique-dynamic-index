#include "graph.h"

Graph::Graph() 
{
    nverts = 0;
    nedges = 0;
}


Graph::Graph(std::string filename, bool binary)
{
    if (binary)
        load_binary(filename);
    else
        load(filename);
}


void Graph::load_binary(std::string filename)
{
    nverts = 0;
    nedges = 0;
    adj_list.clear();
    
    FILE *fp = fopen(filename.c_str(), "rb");
    vertex v;
    vertex size;
    while (fread(&v, sizeof(v), 1, fp) == 1)
    {
        if (fread(&size, sizeof(size), 1, fp) != 1)
            exit(EXIT_FAILURE);
        std::vector<vertex> neighbors(size);
        if (fread(&neighbors[0], sizeof(neighbors[0]), size, fp) != size)
            exit(EXIT_FAILURE);
        adj_list.insert({v,std::unordered_set<vertex>(neighbors.begin(), neighbors.end())});
        nverts ++;
        nedges += size;
    }
    nedges /= 2;    // both (u,v) and (v,u) are stored
    fclose(fp);
}


void Graph::load(std::string filename)
{
    nverts = 0;
    nedges = 0;
    adj_list.clear();
    
    std::string line;
    std::ifstream file(filename);
    while (std::getline(file, line))
    {
        if (line[0] == '#') 
            continue;
        
        char* targetptr;
        vertex u = strtoul(line.c_str(), &targetptr, 10);
        vertex v = strtoul(targetptr, NULL, 10);
        
        if (u == v) // disregard self links
            continue;
        
        nedges ++;
        // Check if the vertex is already in the adjacency list
        if (adj_list.count(u) == 0) 
        {
            adj_list.insert({u,std::unordered_set<vertex>()});
            nverts ++;
        }
        if (adj_list.count(v) == 0) 
        {
            adj_list.insert({v,std::unordered_set<vertex>()});
            nverts ++;
        }
        adj_list[u].insert(v);
        adj_list[v].insert(u);
    }
}


std::unordered_set<vertex> Graph::get_verts()
{
    std::unordered_set<vertex> verts(nverts);
    for (auto kv : adj_list)
        verts.insert(kv.first);
    return verts;
}


void Graph::write_binary(std::string filename)
{
    std::vector<vertex> output;
    for (auto kv : adj_list)
    {
        output.push_back(kv.first);
        output.push_back(kv.second.size());
        output.insert(output.end(), kv.second.begin(), kv.second.end());
    }
    
    FILE *fp = fopen(filename.c_str(), "wb");
    fwrite(&output[0], sizeof(output[0]), output.size(), fp);
    fclose(fp);
}


/* Simply prints the adjacency list to console (useful for debugging) */
void Graph::display()
{
    for (auto kv : adj_list)
    {
        std::cout << kv.first;
        for (auto v : kv.second)
            std::cout << " " << v;
        std::cout << std::endl;
    }
}
