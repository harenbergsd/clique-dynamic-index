#include "query.h"
#include "graph.h"

#pragma once

namespace util
{
    std::vector<Query> load_queries(std::string path);
    Query load_query(std::string filename);
    std::vector<std::string> get_filenames(std::string);
    double elapsed_time(timespec&, timespec&);

    template<typename T>
    void print_vector(std::vector<T> vec)
    {
        for (auto e : vec)
            std::cout << e << " ";
        std::cout << std::endl;
    }
    
    template<typename T>
    void print_set(std::unordered_set<T> set)
    {
        for (auto e : set)
            std::cout << e << " ";
        std::cout << std::endl;
    }
    
    
    inline std::unordered_set<vertex>
    intersection(const std::unordered_set<vertex>& S1, const std::unordered_set<vertex>& S2)
    {
        if (S2.size() < S1.size())
            return intersection(S2, S1);
        
        std::unordered_set<vertex> inter(S1.size());
        for (vertex v : S1)
            if (S2.find(v) != S2.end())
                inter.insert(v);
        return inter;
    }
    

    inline std::unordered_set<vertex>
    difference(std::unordered_set<vertex>& S1, std::unordered_set<vertex>& S2)
    {
        std::unordered_set<vertex> diff(S1);
        if (S1.size() <= S2.size())
        {
            for (vertex v : S1)
                if (S2.find(v) != S2.end())
                    diff.erase(v);
        }
        else // S2.size() < S1.size()
        {
            for (vertex v : S2)
                if (diff.find(v) != diff.end())
                    diff.erase(v);
        }
        return diff;
    }
}
