#include <fstream>
#include <time.h>
#include <functional>
#include <string.h>
#include <math.h>
#include "graph.h"
#include "bk_state.h"
#include "util.h"
#include "query.h"


// Global variables
Graph graph;
std::string data_path       = "";
std::string query_path      = "";
bool print_metrics          = false;
bool suppress_output        = false;
bool no_index               = false;
long unsigned state_counter = 0;


void usage(char* prog_name, const char* more)
{
    std::cerr << more;
    std::cerr << "usage: " << prog_name << " <data_path> [-q query_path] [-v]\n" << std::endl;
    std::cerr << "-q query path" << std::endl;
    std::cerr << "-v this flag indicates to print the metrics" << std::endl;
    std::cerr << "-s this flag indicates to suppress the output" << std::endl;
    std::cerr << "-n this flag indicates to not use the index (compute from scratch for each query)" << std::endl;
    exit(0);
}


void parse_args(int argc, char** argv)
{
    if (argc<1)
        usage(argv[0], "Incorrect number of arguments\n");
    
    for (int i=1; i<argc; i++) 
    {
        if(argv[i][0] == '-') 
        {
            switch(argv[i][1])
            {
                case 'q':
                    query_path = argv[++i];
                    break;
                case 'v':
                    print_metrics = true;
                    break;
                case 's':
                    suppress_output = true;
                    break;
                case 'n':
                    no_index = true;
                    break;
            }
        } 
        else 
        {
            if (data_path == "")
            {
                data_path = argv[i];
                if (data_path.back() == '/')    // if directory, remove the last /
                    data_path = data_path.substr(0, data_path.length()-1);
            }
            else
                usage(argv[0], "More than one filename\n");
        }
    }
    if (data_path == "")
        usage(argv[0], "No input file has been provided\n");
}


/* Generic graph search algorithm */
std::list<BK_State*> search(Query& Q, std::list<BK_State*>& frontier, std::ofstream& ofile)
{
    std::list<BK_State*> finished;
    while (!frontier.empty()) 
    {
        state_counter ++;
        BK_State* state = frontier.back();
        frontier.pop_back();
        
        if (state->is_goal())
        {
            if (! suppress_output)
                state->output(ofile);
            finished.push_back(state);
        }
        else 
        {
            for (BK_State* child : state->successors(Q))
            {
                if (! child->is_valid(Q))
                    finished.push_back(child);
                else
                    frontier.push_back(child);
            }
            delete state;
        }
    }
    return finished;
}
    

int main(int argc, char* argv[])
{
    // Handle arguments
    parse_args(argc, argv);

    // Initialization of key data structures
    graph = Graph(data_path);
    std::unordered_map<vertex,std::vector<BK_State*> > inv_index;
    std::vector<Query> queries = util::load_queries(query_path);
    std::unordered_set<vertex> seeds;
    
    // Initialize timers
    timespec start_query, end_query;
    timespec start_initialize, end_initialize;
    timespec start_index, end_index;

    unsigned int i=0;
    int ndigits = queries.size()<=1 ? 1 : (int)log10(queries.size()-1)+1;
    for (Query Q : queries)
    {
        // Initialize frontier
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_initialize);
        std::list<BK_State*> frontier = BK_State::initialize(Q, inv_index, seeds);
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_initialize);
        
        // Perform query
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_query);
        state_counter = 0;    // reset state counter before each query
        std::string istr = std::to_string(i++);
        std::string output_filename = std::string(ndigits-istr.length(),'0') + istr + ".cliques";
        std::ofstream ofile;
        if (! suppress_output)
            ofile.open(output_filename);
        for (auto it=frontier.begin(); it!=frontier.end();)
        {
            BK_State* state = *it;
            if (state->is_goal())
            {
                if (! suppress_output)
                    state->output(ofile);
                it = frontier.erase(it);
            }
            else
            {
                state->remove_from(inv_index);
                it++;
            }
        }
        std::list<BK_State*> states = search(Q, frontier, ofile);
        if (! suppress_output)
            ofile.close();
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_query);
        
        // Update index if more than one query
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start_index);
        if (i<queries.size() && !no_index)
        {
            for (BK_State* state : states)
            {
                for (vertex v : state->R)
                    inv_index[v].push_back(state);
                for (vertex v : state->P)
                    inv_index[v].push_back(state);
            }
        }
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end_index);
        
        // Print metrics
        std::cout.precision(3);
        if (print_metrics)
        {
            std::cout << std::fixed 
                      << util::elapsed_time(start_initialize, end_initialize) << ","
                      << util::elapsed_time(start_query, end_query) << ","
                      << util::elapsed_time(start_index, end_index) << ","
                      << state_counter << std::endl;
        }
    }
}
