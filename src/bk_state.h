#include <list>
#include <algorithm>
#include "graph.h"
#include "util.h"

#pragma once

class BK_State
{
    public:
        std::unordered_set<vertex> R;   // current clique that is being built up
        std::unordered_set<vertex> P;   // candidate vertices that can be added to R
        std::unordered_set<vertex> X;   // vertices adjacent to R that have enumerated previously 

        /* The initial (root) state of the BK algorithm */
        BK_State(std::string data_path, std::string query_path);
        
        /* A non-root states of the BK algorithm */
        BK_State(std::unordered_set<vertex> &R, std::unordered_set<vertex> &P, std::unordered_set<vertex> &X);
        BK_State(const std::vector<vertex> &R, const std::vector<vertex> &P, const std::vector<vertex> &X);

        ~BK_State();
        
        // The required methods of a State subclass
        inline bool is_goal();
        inline bool is_valid(Query&);
        std::list<BK_State*> successors(Query&);
        void display();
        void output(std::ofstream &file);
        void remove_from(std::unordered_map<vertex,std::vector<BK_State*> >&);
        
        static std::list<BK_State*> initialize(Query&, std::unordered_map<vertex,std::vector<BK_State*> >&, std::unordered_set<vertex>&);
        static std::list<BK_State*> roots_for_seeds(std::unordered_set<vertex>&, std::unordered_set<vertex>&);
        static vertex select_pivot(std::unordered_set<vertex>&, std::unordered_set<vertex>&);
};


bool BK_State::is_valid(Query& Q)
{
    if (Q.verts.size()==0 || Q.eta<=0)
        return true;
    
    vertex count = util::intersection(Q.verts, P).size();
    if (count >= Q.eta)
        return true;
    
    count += util::intersection(R, Q.verts).size();
    return count >= Q.eta;
}


bool BK_State::is_goal()
{
    if (P.size()==0 && X.size()==0)
        return true;
    return false;
}
