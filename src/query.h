#include <unordered_set>
#include "graph.h"

#pragma once

class Query
{
    public:
        std::unordered_set<vertex> verts;
        vertex eta;
};
