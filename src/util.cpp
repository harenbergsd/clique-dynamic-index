#include "util.h"
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>

namespace util
{
    double elapsed_time(timespec& t1, timespec& t2)
    {
        return (t2.tv_sec-t1.tv_sec) +
               (double)(t2.tv_nsec-t1.tv_nsec)/1000000000;
    }

    
    std::vector<Query> load_queries(std::string path)
    {
        std::vector<Query> queries;
        if (path == "")
        {
            queries.push_back(Query());
            return queries;
        }
        std::vector<std::string> qfiles = util::get_filenames(path);
        for (auto qfile : qfiles)
            queries.push_back(util::load_query(qfile));
        return queries;
    }


    Query load_query(std::string filename)
    {
        Query Q;
        std::string line;
        std::ifstream file(filename);
    
        if (! std::getline(file, line)) //eta value is listed first
        {
            Q.eta = 0;
            return Q;
        }
        
        Q.eta = strtoul(line.c_str(), NULL, 10);
        
        // Load the subsequent query vertices
        while (std::getline(file, line))
        {
            vertex v = strtoul(line.c_str(), NULL, 10);
            Q.verts.insert(v);
        }

        return Q;
    }


    /* List filenames in given directory */
    std::vector<std::string> get_filenames(std::string path) {
        // Check if it is a file (not a directory)
        struct stat path_stat;
        stat(path.c_str(), &path_stat);
        if (S_ISREG(path_stat.st_mode))
            return {path};
        
        DIR *dirp = opendir(path.c_str());
        dirent *dp;
        std::vector<std::string> filenames;
        while ((dp = readdir(dirp)) != NULL) {
            if (strcmp(dp->d_name,".") && strcmp(dp->d_name,".."))
                filenames.push_back(path+"/"+dp->d_name);
        }
        sort(filenames.begin(), filenames.end());
        return filenames;
    }
}
