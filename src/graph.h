#include <iostream>
#include <stdio.h>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#pragma once

typedef unsigned vertex;

class Graph 
{
    private:
        std::unordered_map<unsigned, std::unordered_set<unsigned> > adj_list;
    
    public:
        vertex nverts;
        ulong nedges;
        
        Graph();
        Graph(std::string filename, bool binary=true);

        std::unordered_set<vertex> get_verts();
        inline std::unordered_set<vertex>& neighbors(vertex);
        inline vertex degree(vertex);
        void display();
        void write_binary(std::string);
        void load(std::string);
        void load_binary(std::string);
};


inline vertex Graph::degree(vertex v)
{
    return neighbors(v).size();
}

inline std::unordered_set<vertex>& Graph::neighbors(vertex v)
{
    return adj_list[v];
}
